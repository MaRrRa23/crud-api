import { Router } from 'express';
const router = Router();

import { UserControllerInstance } from '../controllers/user.controller.js';

router.use((req, res, next) => { console.log(123) });

router.route('/')
    .get((req, res, next) => UserController.getCurrentUser(req.locals.userId).then().catch())
    .put()
    .delete()

router.route('/tag')
    .post()

router.route('/tag/:id')
    .delete()

router.route('/tag/my')
    .get()

export { router };