import { Router } from "express";
import { readdir } from 'fs/promises';
import * as path from 'path';
import * as route from '../routes/user.js'

const APIRouter = await
    (async () => {
        const routesPath = path.resolve(process.cwd() + '/routes');
        const routeFiles = await readdir(routesPath);

        let modules = {};
        for (const route of routeFiles) {
            if (route !== 'index.js') {
                const pathToRoute = path.resolve(routesPath + `/${route}`);
                const { router: loadedRouter } = await import(pathToRoute);
                modules[route.slice(0, route.length - 3)] = loadedRouter;
            }
        }
        return modules;
    })();

const router = Router();
router.use('/user', APIRouter['user']);

export { router };