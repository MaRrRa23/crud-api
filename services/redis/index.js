import * as redisClient from 'redis';

const redis = redisClient.createClient({ host: 'localhost', port: 6379 });

redis.on('error', e => console.error(e));
redis.connect().then(() => console.log('connected to redis'));

export { redis };