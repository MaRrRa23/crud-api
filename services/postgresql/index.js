import pgPromise from 'pg-promise';
const pgp = pgPromise();
const db = pgp({
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    database: process.env.PG_DATABASE,
    user: process.env.PG_LOGIN,
    password: process.env.PG_PASSWORD
})

try {
    await db.connect();
    console.log(`connected to postgresql at ${process.env.PG_HOST}:${process.env.PG_PORT}`);
} catch (e) {
    console.error(e.message);
    process.exit(1);
}

export {
    db, pgp
};