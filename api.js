import express from 'express';
import 'dotenv/config';
const PORT = process.env.PORT || 8000;
const app = express();

import { router } from "./routes/index.js";

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', router);

app.listen(() => console.log('express is listening on PORT ' + PORT));
